# Vue app demo

This repository contains a demo application running via Nomad.

For working with coscine vue projects, refer to: [vue-template](https://git.rwth-aachen.de/coscine/templates/vue-template)