job "exampleapp" {
  datacenters = ["dc1"]
  
  group "exampleapp" {
    task "exampleapp" {
      driver = "raw_exec"
    
      config {
        command = "C:/Programs/Example/app/start.bat"
        args    = []
      }
    }
  }
}