job "demoApp" {
  datacenters = ["dc1"]
  
  group "demoApp" {
    task "demoApp" {
      driver = "raw_exec"
    
      config {
        command = "C:/Programs/NodeJS/npx.cmd"
        args    = ["serve", "-s", "C:/Programs/DemoApp/"]
      }
	  
	  env {
        PORT = "9999"
      }
    }
  }
}